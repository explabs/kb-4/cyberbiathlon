//= ../../node_modules/jquery/dist/jquery.js
//= ../../node_modules/jquery-smooth-scroll/jquery.smooth-scroll.js
//= ../../node_modules/bootstrap/dist/js/bootstrap.js
//= ../../node_modules/isotope-layout/dist/isotope.pkgd.js
//= ../../node_modules/isotope-masonry-horizontal/masonry-horizontal.js

$(document).ready(function() {
  initScroll();
  // initTooltips();
  initSVG();
  initLogos();
  initResult();
  initAnalytics();
  // initVideo();
});

function initScroll() {
  $(".navbar a").smoothScroll();
  $("a.smoothscroll").smoothScroll();
}

function initTooltips() {
  $('[data-toggle="tooltip"]').tooltip();
}

function initLogos() {
  $(".logos").isotope({
    itemSelector: ".logos__logo",
    percentPosition: true,
    masonry: {
      columnWidth: '.logos__sizer'
    }
  });
}

function initResult() {
  // $(".results").isotope({
  //   layoutMode: 'vertical',
  //   itemSelector: "tbody tr",
  //   getSortData: {
  //     title: ".result__title",
  //     score: ".result__score parseInt",
  //   },
  //   sortBy : 'random'
  // });
}

function initSVG() {
  $('img.svg').each(function(){
    var $img = jQuery(this);
    var imgID = $img.attr('id');
    var imgClass = $img.attr('class');
    var imgURL = $img.attr('src');

    jQuery.get(imgURL, function(data) {
        // Get the SVG tag, ignore the rest
        var $svg = jQuery(data).find('svg');

        // Add replaced image's ID to the new SVG
        if(typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
        }
        // Add replaced image's classes to the new SVG
        if(typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass+' replaced-svg');
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a');

        // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
        if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
            $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
        }

        // Replace image with new SVG
        $img.replaceWith($svg);

    }, 'xml');

});
}

function initMap() {
  var map = new google.maps.Map(document.getElementById("google-map"), {
    zoom: 17,
    center: { lat: 55.669946, lng: 37.480123 },
    tilt: 45
  });

  var directionsService = new google.maps.DirectionsService();
  var directionsDisplay = new google.maps.DirectionsRenderer();

  directionsDisplay.setMap(map);
  directionsService.route(
    {
      origin: { lat: 55.664782, lng: 37.483425 },
      destination: { lat: 55.669946, lng: 37.480123 },
      travelMode: "WALKING"
    },
    function(response, status) {
      if (status === "OK") {
        directionsDisplay.setDirections(response);
      } else {
        console.error("Directions request failed due to " + status);
      }
    }
  );
}

function initAnalytics() {
  $(".gform").on("click", function(e) {
    e.preventDefault();
    alert('Регистрация закрыта!');
    ga("send", "event", "gform", "open");
    console.log("GForm clocked");
    return true;
  });
}

function initVideo() {
  $('video').play();
}
